![alt text](https://www.embl.de/download/zeller/metaG_course_2021/a.png)

# Practical: Taxonomic profiling of 16S and WGS datasets
Objectives of the practical:
- Get an overview about basic characteristics of fastq files
- Learn how to assess read quality and trim/remove low quality reads
- Learn a taxonomic profiling workflow for both, WGS (whole-genome shotgun) and Amplicon (16S) sequencing data

Before we start: 
1) Make sure you activated following conda environment:
```
conda activate /scratch/karcher/envs/metag_course_2022
```
2) Download and unpack the raw data into your folder on /scratch:
```
cd /scratch/<yourDirectory>

curl -OJA "linux" https://oc.embl.de/index.php/s/f0Iarv41bWRfO96/download

tar -xf rawData.tar.gz
```

# Part 1: Taxonomic profiling of WGS samples using mOTUs


## Evaluate the quality of the raw sequencing data

1. Navigate into the folder with the WGS raw data
   <details><summary>SOLUTION</summary>
   <p>

   ```
   cd /scratch/<yourDirectory>/data/WGS/
   ```

2. Look at the fastq files, how are they structured?
   <details><summary>SOLUTION</summary>
   <p>


   When we work with metagenomic data we usually have two fastq files produced by
   the Illumina sequencer:
   - a file containing the forward reads
   - a file containing the reverse reads
   
   Usually the prefix of the file name is the same, and we have `_R1_` for the file
   with forward reads and `_R2_` for the file with reverse reads, example:
   ```
   rawReads_R1.fastq.gz
   rawReads_R2.fastq.gz
   ```
   
   A fastq file contains 4 lines for each read, with the following information:
   
   | Line | Description |
   | ------ | ------ |
   | 1 | A line starting with `@` and the read id |
   | 2 | The DNA sequence | 
   | 3 | A line starting with `+` and sometimes the same information as in line 1 | 
   | 4 | A string of characters that represents the quality score (same number of characters as in line 2) | 
   
   We can have a look at the first read (4 lines) with `zcat rawReads_R1.fastq.gz | head -n 4`:
   ```
   @READNAME
   CTCTAGCAGATACTCTCCCTATATGAACTCATGGGGGCGGGGATGCCCGTCCTGTGTAACAATAAAAAATAACCTTGATGAGGGCGGATAGATCCTACCT
   +
   BBBFFFF<FBFFFFFFIIIIBFBFFBFIIIIFFFIIIIIFFBFFFFFB77BBFFBBBBBBBBBBFFFB7<0BBBB<BBBBFBBBFFF<<7BBBFFFBBBB

   ```

   Each character in the fourth line can be converted to a quality score ([Phred-33](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/QualityScoreEncoding_swBS.htm)) from 1 to 40:
   ```
        Character: !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHI
                   |         |         |         |         |
    Quality score: 0........10........20........30........40 
   ```
   
   And, for each quality score there is an associated probability for correctly calling a base:
   
   | Quality Score | Probability of incorrect base call | Base call accuracy |
   | ------ | ------ | ------ | 
   | 10 | 1 in 10 | 90% |
   | 20 | 1 in 100 | 99% |
   | 30 | 1 in 1000 | 99.9% |
   | 40 | 1 in 10,000 | 99.99% |
   
   </p> 


3. Use `fastqc` ([link](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)) to check the quality of the reads in the two files.
   <details><summary>SOLUTION</summary>
   <p>
 
   Run on the terminal:
   ```
   fastqc rawReads_R1.fastq.gz
   fastqc rawReads_R2.fastq.gz
   ```
   Now open the fastQC report in the browser:
   ```
   google-chrome rawReads_R1_fastqc.html
   google-chrome rawReads_R2_fastqc.html
   ```

   
   This view shows an overview of the range of quality values across all bases at each position in the FastQ file. 
   For each position a BoxWhisker type plot is drawn with:
   - median as a red line,
   - the inter-quartile range (25-75%) as a yellow box,
   - upper and lower whiskers represent the 10% and 90% points,
   - the blue line represents the mean quality.
   
   The y-axis on the graph shows the quality scores. The higher the score the better the base call. The background of the graph divides the y axis into very good quality calls (green), calls of reasonable quality (orange), and calls of poor quality (red). The quality of calls on most platforms will degrade as the run progresses, so it is common to see base calls falling into the orange area towards the end of a read.
   
   </p> 

4. How would you interpret the read quality of the reads?
   <details><summary>SOLUTION</summary>
   <p>
    For this sample, the quality of the reads is pretty good, so we could use it directly for taxonomic profiling via mOTUs.
    
   <p>   


## Profiling using mOTUs
The majority of microbiome studies rely on an accurate identification of the microbes and quantification their abundance in the sample under study, a process called taxonomic profiling.

We would like to save the profile in a file like:
```
Bacteroides_vulgatus    0.34
Prevotella_copri        0.16
Eubacterium_rectale     0.10
...
```

And, if we pull together many samples:
```
                        sample_1  sample_2
Bacteroides_vulgatus        0.34      0.01
Prevotella_copri            0.16      0.42
Eubacterium_rectale         0.10      0.00
```


We use [mOTUs](https://github.com/motu-tool/mOTUs_v2) to create taxonomic profiles of metagenomic samples.
More information can be found also in [this protocol paper](https://currentprotocols.onlinelibrary.wiley.com/doi/full/10.1002/cpz1.218).

5. Use `motus` (manual: [link](https://github.com/motu-tool/mOTUs_v2#simple-examples)) to create a profile from the files that we just explored.
   <details><summary>SOLUTION</summary>
   <p>

   ```
   motus profile -f rawReads_R1.fastq.gz -r rawReads_R2.fastq.gz -o taxProfile.motus -n test_sample -t 8
   ```
   
   Which produces (`head taxProfile.motus`):
   ```
   #consensus_taxonomy	test_sample
   Leptospira alexanderi [ref_mOTU_v25_00001]	0.0000000000
   Leptospira weilii [ref_mOTU_v25_00002]	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v25_00004]	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v25_00005]	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v25_00006]	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v25_00007]	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v25_00008]	0.0000000000
   ```
   In the profile, `ref_mOTU` represent species with a reference genome sequence in NCBI. While `meta_mOTU` and `ext_mOTU` represent species that have not been sequenced yet (they do not have a representative genome in NCBI).
   the `unassigned` at the end of the file (check with `tail taxProfile.motus`), represents species that we know to be present, but are not able to profile. The `unassigned` is useful to have a correct evaluation of the relative abundance of all species (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Explain-the-resulting-profile#-1)).
   </p> 

6. How many species are detected? How many are reference species and how many are unknown species?
   <details><summary>SOLUTION</summary>
   <p>

   We can have a look at the identified species that are different from zero with `cat taxProfile.motus | grep -v "0.0000000000"`:
   ```
   # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 75 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1
   # call: python /nile/DB/milanese/CONDA/miniconda3.9/bin/../share/motus-3.0.1//motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus -t 8
   #consensus_taxonomy	unnamed sample
   Escherichia coli [ref_mOTU_v3_00095]	0.0001797485
   Eggerthella lenta [ref_mOTU_v3_00719]	0.0001633324
   Ruminococcus bromii [ref_mOTU_v3_00853]	0.0049443856
   ```
   
   With `cat taxProfile.motus | grep -v "0.0000000000" | grep -v "#" | wc -l`, we can see that we have 62 detected species.
   
   To count the number of ref-mOTUs you can use:
   ```
   cat taxProfile.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   and for meta-mOTUs:
   ```
   cat taxProfile.motus | grep -v "0.0000000000" | grep -c "meta_mOTU_"
   ```
   and for ext-mOTUs:
   ```
   cat taxProfile.motus | grep -v "0.0000000000" | grep -c "ext_mOTU_"
   ```
   </p> 


7. (Extra) Can you change some parameters in `motus` to profile more or less species? 
   <details><summary>SOLUTION</summary>
   <p>

   In the mOTUs wiki page there are more information: [link](https://github.com/motu-tool/mOTUs_v2/wiki/Increase-precision-or-recall).  
   Basically, you can use `-g` and `-l` to increase(/decrease) the specificity of the detected species. Lower values allows you to profile more species (lax thresholds); while higher values corresponds to stringent cutoffs (and hence less profiled species).  

   If we run:
   ```
   motus profile -f rawReads_R1.fastq.gz -r rawReads_R2.fastq.gz -o taxProfile_lessStringent.motus -t 8 -g 1 -l 40 -n lessStringent
   cat taxProfile_lessStringent.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   We obtain 136 species (more than double compared to default parameters). While, with:
   ```
   motus profile -f rawReads_R1.fastq.gz -r rawReads_R2.fastq.gz -o taxProfile_moreStringent.motus -t 8 -g 6 -l 90 -n moreStringent
   cat taxProfile_moreStringent.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   we only geth 38 species.
   </p> 


8. (Extra) How can you merge different motus profiles into one file?
   Try to merge the three files created in the previous exercises into one profile.
   
   <details><summary>SOLUTION</summary>
   <p>

   You can use `motus merge` (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Merge-profiles-into-a-table)).  
   Note that the name of the samples is speciefied by `-n` when using `motus profile`. 
   
   You can run:
   ```
   motus merge -i taxProfile.motus,taxProfile_lessStringent.motus,taxProfile_moreStringent.motus -o all_samples.motus
   ```
   which looks like (`head all_samples.motus`):
   
   ```
   #consensus_taxonomy	test_sample	lessStringent	moreStringent
   Leptospira alexanderi [ref_mOTU_v25_00001]	0.0000000000	00000000000	0.0000000000
   Leptospira weilii [ref_mOTU_v25_00002]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v25_00004]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v25_00005]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v25_00006]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v25_00007]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v25_00008]	0.0000000000	0.0000000000	0.0000000000
   ```

   We can also check the one that are all different from zero by running:
   ```
   cat all_samples.motus |  grep -vP "0.0000000000\t0.0000000000\t0.0000000000"
   ```


   Note that that if you want to load this table into another tool (for example R), you would like to skip the first 2 rows.
   
   </p> 
Now we have created taxonomic profiles from shotgun sequencing data. Let's do it with 16S Amplicon sequencing data next!


# Part 2: Taxonomic profiling from Amplicon (16S) data
To profile the 16S data, change directory into:
```
cd /scratch/<yourFolder>/data/16S
```

## Evaluation of read quality

1. As a first step, we also want to assess the quality of the raw reads. What do you think?
   <details><summary>SOLUTION</summary>
   <p>

    ```
    fastqc rawReads_R1.fastq.gz
    fastqc rawReads_R2.fastq.gz
    ```

    Now check the fastqc reports:
    
    ```
    google-chrome rawReads_R1_fastqc.html
    google-chrome rawReads_R2_fastqc.html
    ```

   <p>

2. Here, the quality of the base calls at the end of the reads gets very bad. Therefore, we are removing low-quality bases at the beginning/end of the reads using `trimmomatic` ([trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)).
   <details><summary>SOLUTION</summary>
   <p>

   ```
   trimmomatic PE rawReads_R1.fastq.gz rawReads_R2.fastq.gz LEADING:20 TRAILING:20 SLIDINGWINDOW:4:15 MINLEN:100 -baseout filteredReads
   ```
   Which produces 4 files:
   - `filteredReads_1P`, containing the forward reads that pass the filter and have a mate (in `filteredReads_2P`);
   - `filteredReads_1U`, containing the forward reads that pass the filter and do not have a mate (the paired reverse read didn't pass the filter)
   - `filteredReads_2P`, containing the reverse reads that pass the filter and have a mate (in `filteredReads_1P`);
   - `filteredReads_2U`, containing the reverse reads that pass the filter and do not have a mate (the paired forward read didn't pass the filter)
   <p> 

3. Check the filtered reverse reads with `fastqc`. Has the read quality improved?
   <details><summary>SOLUTION</summary>
   <p>

   ```
   fastqc filteredReads_1P
   fastqc filteredReads_2P
   ```
   ```
   google-chrome filteredReads_1P_fastqc.html
   google-chrome filteredReads_2P_fastqc.html
   ```
 Now we are ready to perform taxonomic annotation of the 16S.

 ## Taxonomic annotation
 To annotate the 16S data we are using [MAPseq](https://github.com/jfmrod/MAPseq). The tool classifies sequencing reads by k-mer matching against a reference set of full-length ribosomal RNA sequences with known taxonomies. For more details, see the [publication](https://github.com/jfmrod/MAPseq).

4. Since `MAPseq` works on fasta samples wee need to convert the qualiy-filtered fastq files into fasta format. What is the difference between the two? Is there information lost?
Use `seqtk`(https://github.com/lh3/seqtk) to convert the fastq files into fasta format.
   <details><summary>SOLUTION</summary>
   <p>
   
   ```
   seqtk seq -a filteredReads_1P > filteredReads_R1P.fasta
   seqtk seq -a filteredReads_2P > filteredReads_R2P.fasta
   ```
   Now look into the fasta files, whats the difference?
   ```
   head filteredReads_R1P.fasta
   head filteredReads_R2P.fasta
   ```
   <p> 

5. Now we can run MAPseq on the profiles:
   <details><summary>SOLUTION</summary>
   <p>
   
   ```
   /scratch/karcher/mapseq-1.2.6-linux/mapseq --outfmt simple filteredReads_R1P.fasta > taxProfile_R1.mseq
   /scratch/karcher/mapseq-1.2.6-linux/mapseq --outfmt simple filteredReads_R2P.fasta > taxProfile_R2.mseq
   ```
   Check the output file:
   ```
   head taxProfile_R1.mseq
   ```

   ```
   # mapseq v1.2.6 (Mar 24 2020)
   #query	dbhit	bitscore	identity	matches	mismatches	gaps	query_start	query_end	dbhit_start	dbhit_end	strand		NCBIv2.2b		MAP_hOTUs	
   ERR475531.166	JF141101:1..1390	226	0.9556451439857483	237	11	0	0	248	528	776	+		Bacteria;Firmicutes;Negativicutes;Veillonellales;Veillonellaceae;Dialister		B16S;90_388	
   ERR475531.11578	KF843045:1..1352	249	0.9960159659385681	250	1	0	0	251	501	752	+		Bacteria;Firmicutes;Clostridia;Clostridiales;Ruminococcaceae		B16S	
   ERR475531.357	KF842592:1..1369	207	0.912350594997406	229	22	0	0	251	501	752	+		Bacteria;Firmicutes;Clostridia;Clostridiales		B16S	
   ERR475531.11747	DQ807207:1..1392	244	1	244	0	0	0	244	515	759	+		Bacteria;Firmicutes;Clostridia;Clostridiales		B16S;90_17229;96_87647;97_112036;98_149269;99_220667	
   ERR475531.387	KF842203:1..1417	248	0.9959999918937683	249	1	0	0	250	524	774	+		Bacteria;Bacteroidetes;Bacteroidia;Bacteroidales;Bacteroidaceae;Bacteroides		B16S;90_13557;96_44766;97_56115;98_72621;99_102278	
   ERR475531.11832	EU463002:1..1372	157	1	157	0	0	1	158	504	661	+		Bacteria;Firmicutes;Clostridia;Clostridiales;Ruminococcaceae		B16S;90_519	
   ERR475531.11912	DQ793576:1..1392	247	0.9920318722724915	249	2	0	0	251	516	767	+		Bacteria;Firmicutes;Clostridia;Clostridiales		B16S;90_20	
   ERR475531.460	HQ762993:1..1449	155	0.9144384860992432	171	16	0	0	187	483	670	+		Bacteria;Firmicutes;Clostridia;Clostridiales	
   ```

   <p>

Now we have taxonomically annotated the individual reads. In the next step, we will switch to RStudio and convert this into a relative abundance matrix (e.g. as the one we generated from WGS data using mOTUs)



## References and additional information

1. [Intro to FastQC with explanation of a FASTA file](https://hbctraining.github.io/Intro-to-rnaseq-hpc-O2/lessons/02_assessing_quality.html)
2. [Per base sequence quality fastQC plot](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/3%20Analysis%20Modules/2%20Per%20Base%20Sequence%20Quality.html)
3. [Trimmomatic manual](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf)