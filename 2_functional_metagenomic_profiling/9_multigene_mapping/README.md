# Practical: Metagenomics and multi-gene systems
Objectives of the practical:
- Run GECCO to detect biosynthetic gene clusters (BGCs)
- Match BGC genes to a gene catalog
- Count raw abundance of the BGCs

Before we start: 
1) Make sure you activated following conda environment:
```console
$ source /scratch/karcher/etc/profile.d/conda.sh
$ conda activate /scratch/karcher/envs/metag_course_2022
```
2) Download and unpack the 
```console
cd /scratch/<yourDirectory>
curl -OJA "linux" https://oc.embl.de/index.php/s/f0Iarv41bWRfO96/download
tar -xf rawData.tar.gz
```

# Preliminary data

In the taxonomic profiling part, we generated a mOTUs report for the WGS data.
The most abundant species were the following:
```
Dorea longicatena [ref_mOTU_v2_4203] 0.0285551989
Eubacterium rectale [ref_mOTU_v2_1416] 0.0318254086
Bacteroidales sp. [ref_mOTU_v2_1074] 0.0318828825
unknown Roseburia [meta_mOTU_v2_5354] 0.0370557397
Ruminococcus bromii [ref_mOTU_v2_4720] 0.0459235937
```

Unfortunately, we cannot really use the entirety of a gene catalog for 
this practical, because the alignment time and resources will be too large.
For the sake of the exercise, we will be using the genome from the 
most abundant species (*R. bromii*) and use its genes as our catalog.


## Download data

1. Go to [proGenomes](https://progenomes.embl.de/genome.cgi) (progenomes.embl.de) and find the genome of *Ruminococcus bromii*:
   <details><summary>SOLUTION</summary>
   <p>

   ![progenomes1](png/progenomes1.png)


2. Download the contigs, the genes, and the proteins for *Ruminococcus bromii*:
   <details><summary>SOLUTION</summary>
   <p>

   ![progenomes2](png/progenomes2.png)


3. Move the downloaded sequences to your folder on `/scratch`:
   <details><summary>SOLUTION</summary>
   <p>

   ```console
   cd /scratch/<yourDirectory>/
   mkdir genomes
   mv -v ~/Downloads/40518.SAMEA5851987.* -t genomes
   ```

4. Unzip all sequences:
   <details><summary>SOLUTION</summary>
   <p>

   ```console
   gunzip -v genomes/*.gz
   ```

# Part 1: Detecting BGCs

Two complementary methods for detecting BGCs are [antiSMASH](https://antismash.secondarymetabolites.org), which uses rules, and [GECCO](https://gecco.embl.de/) (gecco.embl.de), which uses a sequence learning model.


## Install GECCO

1. You can check if GECCO is installed with
   ```console
   gecco --version
   ```

2. (Optional): If GECCO is missing, install it with `pip`:
   ```console
   python3 -m pip install gecco-tool
   ```

3. Fix a dependency issue
   ```
   python3 -m pip install "importlib-metadata<5"
   ```

## Find BGCs in the reference genome

1. Run GECCO
   ```console
   gecco -v run -g genomes/40518.SAMEA5851987.contigs.fasta -o bgcs
   ```

   <details><summary>SOLUTION</summary>
   <p>

   ![gecco1](png/gecco1.png)

2. Rerun GECCO with a lower probability threshold
   ```console
   gecco -v run -g genomes/40518.SAMEA5851987.contigs.fasta -o bgcs -m 0.5
   ```

   <details><summary>SOLUTION</summary>
   <p>

   ![gecco1](png/gecco2.png)


3. Inspect the resulting BGC

   <details><summary>SOLUTION</summary>
   <p>

   ![bgc](png/bgc.png)



# Part 2: Map BGC genes to the gene catalog


## Extract proteins from the GECCO predicted BGC

1. Use the GECCO `convert` command to get the proteins from the predicted BGC:

   ```console
   gecco convert gbk -i bgcs -f faa
   ```

   <details><summary>SOLUTION</summary>
   <p>

   You should now have a FASTA file with the amino-acid sequences of the 
   proteins of the BGC:

   ```console
   ls bgcs
   ```
   ```
   drwxr-xr-x 2 martin.larralde_embl users 4.0K Nov 17 08:42 .
   drwxr-xr-x 6 martin.larralde_embl users 4.0K Nov 17 08:47 ..
   -rw-r--r-- 1 martin.larralde_embl users 9.9K Nov 17 09:12 40518.SAMEA5851987.CABMMZ010000069_cluster_1.faa
   -rw-r--r-- 1 martin.larralde_embl users  67K Nov 17 09:04 40518.SAMEA5851987.CABMMZ010000069_cluster_1.gbk
   -rw-r--r-- 1 martin.larralde_embl users 1.5K Nov 17 09:04 40518.SAMEA5851987.contig.clusters.tsv
   -rw-r--r-- 1 martin.larralde_embl users 413K Nov 17 09:04 40518.SAMEA5851987.contig.features.tsv
   -rw-r--r-- 1 martin.larralde_embl users 256K Nov 17 09:04 40518.SAMEA5851987.contig.genes.tsv
   ```

## Map the proteins to the gene catalog

1. Use BLASTp to find to which catalog gene each BGC protein corresponds:

   ```
   blastp -query bgcs/40518.SAMEA5851987.CABMMZ010000069_cluster_1.faa -subject genomes/40518.SAMEA5851987.proteins.fasta -outfmt 7 -max_target_seqs 1 -out bgc.blastbl
   ```

   <details><summary>SOLUTION</summary>
   <p>

   Fields are the following:

   ```
   # Fields: query acc., subject acc., % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
   ```

   ```
   40518.SAMEA5851987.CABMMZ010000069_26	40518.SAMEA5851987.GCA_902387655_01414	100.000	101	0	0	1	101	1	101	3.94e-70	200
   40518.SAMEA5851987.CABMMZ010000069_27	40518.SAMEA5851987.GCA_902387655_01415	100.000	247	0	0	1	247	1	247	1.42e-179	489
   40518.SAMEA5851987.CABMMZ010000069_28	40518.SAMEA5851987.GCA_902387655_01416	100.000	132	0	0	1	132	1	132	1.50e-94	265
   40518.SAMEA5851987.CABMMZ010000069_29	40518.SAMEA5851987.GCA_902387655_01417	100.000	102	0	0	1	102	1	102	4.61e-71	203
   40518.SAMEA5851987.CABMMZ010000069_30	40518.SAMEA5851987.GCA_902387655_01418	100.000	586	0	0	1	586	1	586	0.0	1178
   40518.SAMEA5851987.CABMMZ010000069_31	40518.SAMEA5851987.GCA_902387655_01419	100.000	2443	0	0	1	2443	1	2443	0.0	5030
   40518.SAMEA5851987.CABMMZ010000069_32	40518.SAMEA5851987.GCA_902387655_01420	100.000	179	0	0	1	179	1	179	2.19e-127	351
   40518.SAMEA5851987.CABMMZ010000069_33	40518.SAMEA5851987.GCA_902387655_01421	100.000	235	0	0	1	235	1	235	5.65e-175	476
   40518.SAMEA5851987.CABMMZ010000069_34	40518.SAMEA5851987.GCA_902387655_01422	100.000	425	0	0	1	425	1	425	0.0	849
   40518.SAMEA5851987.CABMMZ010000069_35	40518.SAMEA5851987.GCA_902387655_01423	100.000	174	0	0	1	174	1	174	4.36e-130	358
   40518.SAMEA5851987.CABMMZ010000069_36	40518.SAMEA5851987.GCA_902387655_01427	100.000	181	0	0	1	181	1	181	1.22e-126	350
   40518.SAMEA5851987.CABMMZ010000069_37	40518.SAMEA5851987.GCA_902387655_01428	100.000	676	0	0	1	676	1	676	0.0	1399
   40518.SAMEA5851987.CABMMZ010000069_38	40518.SAMEA5851987.GCA_902387655_01429	100.000	240	0	0	1	240	1	240	1.59e-180	491
   40518.SAMEA5851987.CABMMZ010000069_39	40518.SAMEA5851987.GCA_902387655_01430	100.000	437	0	0	1	437	1	437	0.0	899
   40518.SAMEA5851987.CABMMZ010000069_40	40518.SAMEA5851987.GCA_902387655_01431	100.000	340	0	0	1	340	1	340	0.0	702
   40518.SAMEA5851987.CABMMZ010000069_41	40518.SAMEA5851987.GCA_902387655_01432	100.000	353	0	0	1	353	1	353	0.0	738
   40518.SAMEA5851987.CABMMZ010000069_42	40518.SAMEA5851987.GCA_902387655_01433	100.000	228	0	0	1	228	1	228	1.38e-170	464
   40518.SAMEA5851987.CABMMZ010000069_43	40518.SAMEA5851987.GCA_902387655_01434	100.000	80	0	0	1	80	1	80	1.91e-55	162
   40518.SAMEA5851987.CABMMZ010000069_44	40518.SAMEA5851987.GCA_902387655_01435	100.000	732	0	0	1	732	1	732	0.0	1487
   40518.SAMEA5851987.CABMMZ010000069_45	40518.SAMEA5851987.GCA_902387655_01436	100.000	244	0	0	1	244	1	244	0.0	495
   40518.SAMEA5851987.CABMMZ010000069_46	40518.SAMEA5851987.GCA_902387655_01437	100.000	151	0	0	1	151	1	151	1.97e-111	309
   40518.SAMEA5851987.CABMMZ010000069_47	40518.SAMEA5851987.GCA_902387655_01438	100.000	234	0	0	1	234	1	234	3.10e-179	487
   ```

2. Extract the list of catalog genes for the BGC:

   ```
   grep -v '#' bgc.blastbl | cut -f2 > bgc_genes.txt
   ```

   <details><summary>SOLUTION</summary>
   <p>

   ```
   40518.SAMEA5851987.GCA_902387655_01414
   40518.SAMEA5851987.GCA_902387655_01415
   40518.SAMEA5851987.GCA_902387655_01416
   40518.SAMEA5851987.GCA_902387655_01417
   40518.SAMEA5851987.GCA_902387655_01418
   40518.SAMEA5851987.GCA_902387655_01419
   40518.SAMEA5851987.GCA_902387655_01420
   40518.SAMEA5851987.GCA_902387655_01421
   40518.SAMEA5851987.GCA_902387655_01422
   40518.SAMEA5851987.GCA_902387655_01423
   40518.SAMEA5851987.GCA_902387655_01427
   40518.SAMEA5851987.GCA_902387655_01428
   40518.SAMEA5851987.GCA_902387655_01429
   40518.SAMEA5851987.GCA_902387655_01430
   40518.SAMEA5851987.GCA_902387655_01431
   40518.SAMEA5851987.GCA_902387655_01432
   40518.SAMEA5851987.GCA_902387655_01433
   40518.SAMEA5851987.GCA_902387655_01434
   40518.SAMEA5851987.GCA_902387655_01435
   40518.SAMEA5851987.GCA_902387655_01436
   40518.SAMEA5851987.GCA_902387655_01437
   40518.SAMEA5851987.GCA_902387655_01438
   ```

3. Get the counts for these genes from the read mapping results:
   ```
   grep -f bgc_genes.txt genecounts.txt
   ```

   <details><summary>SOLUTION</summary>
   <p>

   ```
   1 40518.SAMEA5851987.GCA_902387655_01431
   1 40518.SAMEA5851987.GCA_902387655_01437
   2 40518.SAMEA5851987.GCA_902387655_01416
   2 40518.SAMEA5851987.GCA_902387655_01427
   3 40518.SAMEA5851987.GCA_902387655_01417
   3 40518.SAMEA5851987.GCA_902387655_01422
   3 40518.SAMEA5851987.GCA_902387655_01433
   4 40518.SAMEA5851987.GCA_902387655_01432
   5 40518.SAMEA5851987.GCA_902387655_01430
   5 40518.SAMEA5851987.GCA_902387655_01434
   6 40518.SAMEA5851987.GCA_902387655_01436
   7 40518.SAMEA5851987.GCA_902387655_01415
   8 40518.SAMEA5851987.GCA_902387655_01429
   8 40518.SAMEA5851987.GCA_902387655_01438
   12 40518.SAMEA5851987.GCA_902387655_01435
   13 40518.SAMEA5851987.GCA_902387655_01428
   17 40518.SAMEA5851987.GCA_902387655_01418
   47 40518.SAMEA5851987.GCA_902387655_01419
   ```


## References and additional information

1. [Accurate de novo identification of biosynthetic gene clusters with GECCO. Laura M Carroll, Martin Larralde, Jonas Simon Fleck, Ruby Ponnudurai, Alessio Milanese, Elisa Cappio Barazzone & Georg Zeller. bioRxiv (2021)](https://www.biorxiv.org/content/10.1101/2021.05.03.442509v1)
