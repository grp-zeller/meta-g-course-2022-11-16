# metaG_course

Materials for the metagenomic bioinformatic course Nov 2022

# Schedule

## Day 1 (2022-11-16)

| Time          | Lesson                          |
|---------------|---------------------------------|
| 9:00 - 9:15 | Introduction |
| 9:15 - 11:00 | Next Generaption sequencing overview |
| 11:30 - 12:30  | Taxonomic profiling (lecture)   |
| 12:30 - 13:30 | Lunch |
| 13:30 - 14:00 | Setting up environment|
| 14:00 - 15:00 | Taxonomic profiling (practical) |
| 15:00 | Q&A + bring-your-own-data       |

## Day 2 (2022-11-17)

| Time          | Lesson                               |
|---------------|--------------------------------------|
| 9:00 - 10:00  | Comparative metagenomics (lecture)   |
| 10:00 - 12:00 | Comparative metagenomics (practical) |
| 12:00 - 13:00 | Lunch |
| 13:00 - 14:00 | Q&A + bring-your-own-data            |
| 14:00 - 15:00  | Functional profiling (lecture)   |
| 15:15 - 16:45 | Functional profiling (practical) |
| 17:00 | Q&A + bring-your-own-data        |


# Preparations 

## Course data

In order to get the data that we will analyze during the course, 
you can download them via a bash terminal (see below)
```bash
wget https://www.embl.de/download/zeller/metaG_course/course_material.zip
unzip course_material.zip
```
or by just clicking 
[this link in your browser](https://www.embl.de/download/zeller/metaG_course/course_material.zip)

## Software requirment

1. You will need a bash terminal. If you are using Windows, you can install [cygwin](https://www.cygwin.com/), if you have Linux or MacOs it will be pre-installed (just open the Terminal).
2. You need to install conda. If you don't have it already you can type within the terminal:    
     For windows: Install miniconda ([link](https://docs.conda.io/en/latest/miniconda.html)) and add it to your path (in cygwin).  
     For MacOS/Linux:  
     `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`    
     `chmod +x Miniconda3-latest-Linux-x86_64.sh `    
     `./Miniconda3-latest-Linux-x86_64.sh`    
     Say `yes` to install and provide a path to a directory where to install conda.
     Add `conda/bin` to the path.

3. Install the following packages with conda:
    - trimmomatic (`conda install -c bioconda trimmomatic`)
    - fastqc (`conda install -c bioconda fastqc`)
    - mOTUs2 (`conda install -c bioconda motus`)
    - HMMER3 (`conda install -c bioconda hmmer`)
    - muscle (`conda install -c bioconda muscle`)
    - jalview (`conda install -c bioconda jalview`)

4. Please further make sure you have R and Rstudio installed. 
5. Please install the following R packages by running the following commands from within Rstudio:
```R
install.packages("devtools")
devtools::install_github("zellerlab/siamcat")
install.packages("ape")
install.packages("labdsv")
install.packages("tidyverse")
install.packages("vegan")
install.packages("rjson")
install.packages("matrixStats")

install.packages("pheatmap")
```

# Contact

Please feel free to contact us if you have any other questions:

* nicolai.karcher[at]embl.de
* fabian.springer[at]embl.de
* martin.larralde[at]embl.de
* georg.zeller[at]embl.de

